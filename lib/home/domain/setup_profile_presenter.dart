import 'package:supabase_flutter/supabase_flutter.dart';

import '../data/models/model_platform.dart';
import '../data/repository/supabase.dart';

class SetupProfilePresenter{

  List<ModelPlatform> platforms = [];

  Future<void> pressConfirm(
      String fullname,
      String phone,
      Function() onConfirm,
      Function(String) onError
      ) async {
    try{
      await saveUserData(fullname, phone);
      onConfirm();
    } on AuthException catch(e){
      onError(e.message);
    } on PostgrestException catch(e){
      onError(e.message);
    } on Exception catch(e){
      onError(e.toString());
    }
  }
}