import 'package:supabase_flutter/supabase_flutter.dart';

import '../models/model_platform.dart';
import '../models/model_profile.dart';

var supabase = Supabase.instance.client;

Future<void> saveUserData(
    String fullname,
    String phone
    ) async {
  await supabase.auth.updateUser(
      UserAttributes(
          data: {
            "fullname": fullname,
            "phone": phone
          }
      )
  );
}

Future<List<ModelPlatform>> getAllPlatforms() async {
  var response = await supabase.from("platforms").select();
  return response.map(
          (Map<String, dynamic> e) => ModelPlatform.fromJson(e)
  ).toList();
}

ModelProfile getModelProfile() {
  var userMetadata = supabase.auth.currentUser?.userMetadata!;
  var platforms = loadPlatforms();
  return ModelProfile(
      fullname: userMetadata?["fullname"],
      phone: userMetadata?["phone"],
      birthday: userMetadata?["birthday"],
      avatar: userMetadata?["avatar"],
      platforms: platforms,
  );
}

Future<ModelPlatform> getPlatform(String id) async {
  var response = await supabase
      .from("platforms")
      .select()
      .eq("id", id)
      .single();
  return ModelPlatform.fromJson(response);
}

List<ModelPlatform> loadPlatforms() {
  List<dynamic> shortData = supabase.auth.currentUser!.userMetadata!["platforms"];

  List<ModelPlatform> platforms = shortData.map((e) =>
      ModelPlatform.fromJson(e)
  ).toList();

  return platforms;
}
