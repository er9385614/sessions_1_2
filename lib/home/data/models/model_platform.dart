class ModelPlatform{
  final String id;
  final String icon;
  final String title;
  String channels;
  final String availableChannels;

  ModelPlatform(
      {
        required this.id,
        required this.icon,
        required this.title,
        required this.availableChannels,
        required this.channels
      });

  bool isValid(){
    var userChannels = channels.split(", ");
    return availableChannels.split(", ").toSet().containsAll(userChannels);
  }

  String getFullIcon(){
    return icon;
  }

  static ModelPlatform fromJson(Map<String, dynamic> json){
    return ModelPlatform(
        id: json["id"],
        icon: json["icon"],
        title: json[ "title"],
        availableChannels: json["allow_channels"],
        channels: json["default_channels"]);
  }

  Map<String, String> toJson(){
    return {
      "id": id,
      "title": title,
      "icon": icon,
      "default_channels": channels,
      "allow_channels": availableChannels,
    };
  }
}