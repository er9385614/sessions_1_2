import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sessions_1_2/common/app.dart';
import 'package:sessions_1_2/common/colors.dart';
import 'package:sessions_1_2/common/widgets/CustomTextField.dart';
import 'package:sessions_1_2/home/domain/choose_platform_presenter.dart';
import 'package:sessions_1_2/home/domain/setup_profile_presenter.dart';
import 'package:sessions_1_2/home/presentation/pages/choose_platform_page.dart';
import 'package:sessions_1_2/home/presentation/pages/home_page.dart';

class SetupProfilePage extends StatefulWidget{
  @override
  State<SetupProfilePage> createState() => _SetupProfilePageState();
}

class _SetupProfilePageState extends State<SetupProfilePage> {

  var presenter = SetupProfilePresenter();
  var fio = TextEditingController();
  var phone = MaskedTextController(mask: "+7 (000) 000 00 00");

  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 73,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  width: 45,
                  height: 45,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(14),
                  ),
                  child: OutlinedButton(
                    style: OutlinedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(14)
                      ),
                      padding: EdgeInsets.symmetric(
                        horizontal: 12,
                        vertical: 12
                      ),
                      minimumSize: Size.zero,
                      side: BorderSide(
                        color: colors.accent
                      )
                    ),
                      onPressed: (){},
                      child: Icon(Icons.add_photo_alternate_outlined,
                      color: colors.accent,)),
                ),
                Container(
                  width: 151,
                  height: 151,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(32)
                  ),
                  child: Image.asset("assets/avatar.png"),
                ),
                Container(
                  width: 45,
                  height: 45,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(14),
                  ),
                  child: OutlinedButton(
                      style: OutlinedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(14)
                          ),
                          padding: EdgeInsets.symmetric(
                              horizontal: 12,
                              vertical: 12
                          ),
                          minimumSize: Size.zero,
                          side: BorderSide(
                              color: colors.accent
                          )
                      ),
                      onPressed: (){},
                      child: Icon(Icons.sunny,
                        color: colors.accent,)),
                ),
              ],
            ),
            CustomTextField(
                label: "ФИО",
                hint: "Введите ваше ФИО",
                controller: fio),
            CustomTextField(
                label: "Телефон",
                hint: "+7 (000) 000 00 00",
                controller: phone),
            SizedBox(height: 24,),
            Text("Дата рождения",
              style: Theme.of(context).textTheme.titleMedium,),
            SizedBox(height: 8,),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                border: Border.all(
                  color: colors.subText
                )
              ),
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
              width: double.infinity,
              height: 44,
              child: Text("Дата рождения",
                style: Theme.of(context).textTheme.titleMedium?.copyWith(
                  color: colors.text
                ),
              ),
            ),
            SizedBox(height: 24,),
            Text("Источники новостей",
              style: Theme.of(context).textTheme.titleMedium,),
            SizedBox(height: 8,),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  border: Border.all(
                      color: colors.subText
                  )
              ),
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
              width: double.infinity,
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text("Выберите платформы",
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                color: colors.text
                            )
                        ),
                      ),
                      GestureDetector(
                          onTap: (){
                            Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (_) => ChoosePlatformPage(
                                      alreadySelectedPlatforms: presenter.platforms,
                                    )
                                ),
                            );
                          },
                          child: Container(
                              width: 14,
                              height: 14,
                              child: Image.asset("assets/add.png"))
                      )
                    ]
                  ),
                ]
              ),
            ),
            SizedBox(height: 24,),
            SizedBox(
                width: double.infinity,
                height: 46,
                child: FilledButton(
                    onPressed: (){
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                              builder: (_) => const HomePage(initialIndex: 3,)
                          ),
                              (route) => false);
                    },
                    child: Text("Продолжить"))
            ),
            SizedBox(height: 32,)
          ],
        ),
      ),
    );
  }
}