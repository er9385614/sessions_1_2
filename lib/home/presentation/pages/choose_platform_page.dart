import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../common/app.dart';
import '../../data/models/model_platform.dart';
import '../../domain/choose_platform_presenter.dart';

class ChoosePlatformPage extends StatefulWidget {
  final List<ModelPlatform> alreadySelectedPlatforms;

  const ChoosePlatformPage(
      {super.key, required this.alreadySelectedPlatforms});

  @override
  State<ChoosePlatformPage> createState() => _ChoosePlatformPageState();
}

class _ChoosePlatformPageState extends State<ChoosePlatformPage> {
  var presenter = ChoosePlatformPresenter();

  @override
  void initState() {
    super.initState();
    presenter.fetchListPlatforms(widget.alreadySelectedPlatforms, (data) {
      setState(() {
        presenter.platforms = data;
      });
    }, (p0) => null);
  }

  List<Widget> getPlatformsWidget() {
    var colors = MyApp.of(context).getColorsApp(context);
    List<Widget> widgets = [];
    for (var model in presenter.platforms.keys) {
      bool isSelect = presenter.platforms[model]!;
      widgets.add(Column(
          children: [
            SizedBox(height: 28,),
        Row(
          children: [
            Image.network(model.getFullIcon(), width: 30, height: 30),
            const SizedBox(width: 8),
            Expanded(
              child: Text(model.title,
                  style: TextStyle(
                      color: colors.text,
                      fontSize: 16,
                      fontWeight: FontWeight.w500)),
            ),
            GestureDetector(
                onTap: () {
                  setState(() {
                    if (isSelect) {
                      presenter.unselectedPlatform(model);
                    } else {
                      presenter.selectedPlatform(model);
                    }
                  });
                },
                child: Container(
                  width: 14,
                  height: 14,
                  child: Image.asset(
                      (isSelect) ? "assets/check.png" : "assets/add.png"),
                ))
          ],
        )
      ]));
    }
    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 22),
          child: Column(
            children: <Widget>[
                  const SizedBox(
                    height: 67,
                  ),
                  Row(
                    children: [
                      GestureDetector(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: SizedBox(
                              width: 14,
                              height: 14,
                              child: Image.asset("assets/remove.png"))),
                      const SizedBox(
                        width: 16,
                      ),
                      Text(
                        "Выберите платформу",
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontFamily: "Roboto",
                            fontSize: 22,
                            color: colors.text),
                      )
                    ],
                  )
                ] +
                getPlatformsWidget(),
          )),
    );
  }
}
