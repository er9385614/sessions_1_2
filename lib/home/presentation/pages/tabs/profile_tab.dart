import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sessions_1_2/common/app.dart';
import 'package:sessions_1_2/common/colors.dart';
import 'package:sessions_1_2/home/presentation/pages/setup_profile_page.dart';

class ProfileTab extends StatefulWidget{
  @override
  State<ProfileTab> createState() => _ProfileTabState();
}

class _ProfileTabState extends State<ProfileTab> {
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 73,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  width: 121,
                  height: 121,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(32)
                  ),
                  child: Image.asset("assets/avatar.png"),
                ),
                SizedBox(width: 18,),
                Expanded(
                  child: Text("Смирнов Александр Максимович",
                  style: TextStyle(
                    color: colors.text,
                    fontWeight: FontWeight.w500,
                    fontSize: 18,
                    fontFamily: "Roboto"
                  ),),
                )
              ],
            ),
            SizedBox(
              height: 24,
            ),
            Divider(height: 1, color: colors.subText,),
            SizedBox(height: 25,),
            Text("История",
            style: TextStyle(
              color: colors.text,
              fontSize: 18,
              fontWeight: FontWeight.w600,
              fontFamily: "Roboto"
            ),),
            SizedBox(height: 18,),
            Text("Прочитанные статьи",
              style: TextStyle(
                  color: colors.text,
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  fontFamily: "Roboto"
              ),),
            SizedBox(height: 18,),
            Text("Чёрный список",
              style: TextStyle(
                  color: colors.text,
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  fontFamily: "Roboto"
              ),),
            SizedBox(height: 25,),
            Text("Настройки",
              style: TextStyle(
                  color: colors.text,
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                  fontFamily: "Roboto"
              ),),
            SizedBox(height: 18,),
            GestureDetector(
              onTap: (){
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                        builder: (_) => SetupProfilePage()
                ),
                        (route) => false);
              },
              child: Text("Редактирование профиля",
                style: TextStyle(
                    color: colors.text,
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    fontFamily: "Roboto"
                ),),
            ),
            SizedBox(height: 18,),
            Text("Политика конфиденциальности",
              style: TextStyle(
                  color: colors.text,
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  fontFamily: "Roboto"
              ),),
            SizedBox(height: 18,),
            Text("Оффлайн чтение",
              style: TextStyle(
                  color: colors.text,
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  fontFamily: "Roboto"
              ),),
            SizedBox(height: 18,),
            Text("О нас",
              style: TextStyle(
                  color: colors.text,
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  fontFamily: "Roboto"
              ),),
            SizedBox(height: 24),
            SizedBox(
                width: double.infinity,
                height: 46,
                child: OutlinedButton(
                  style: OutlinedButton.styleFrom(
                    side: BorderSide(
                      color: colors.error
                    )
                  ),
                    onPressed: (){
                    },
                    child: Text("Выход",
                    style: TextStyle(
                      color: colors.error
                    ),))
            ),

            SizedBox(height: 32,)
          ],
        ),
      ),
    );
  }
}