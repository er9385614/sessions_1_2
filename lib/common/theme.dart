import 'package:flutter/material.dart';
import 'package:sessions_1_2/common/colors.dart';

var light = LightColors();

var lightTheme = ThemeData(
  textTheme: TextTheme(
    titleLarge: TextStyle(
    color: light.text,
    fontSize: 24,
    fontFamily: "Roboto",
    fontWeight: FontWeight.w500
    ),
    titleMedium: TextStyle(
    color: light.subText,
    fontSize: 14,
    fontFamily: "Roboto",
    fontWeight: FontWeight.w400
    ),
    titleSmall: TextStyle(
    color: light.disableTextAccent,
    fontSize: 16,
    fontFamily: "Roboto",
    fontWeight: FontWeight.w400
    ),
  ),
  filledButtonTheme: FilledButtonThemeData(
    style: FilledButton.styleFrom(
      textStyle: TextStyle(
        color: light.disableTextAccent, fontWeight: FontWeight.w700,
        fontSize: 16
      ),
      backgroundColor: light.accent,
      disabledBackgroundColor: light.disableAccent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4)
      ),
      padding: EdgeInsets.symmetric(horizontal: 36, vertical: 15)
    )
  ),
  outlinedButtonTheme: OutlinedButtonThemeData(
    style: OutlinedButton.styleFrom(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4)
        ),
        padding: EdgeInsets.symmetric(horizontal: 36, vertical: 15)
    )
  )
);

var dark = LightColors();

var darkTheme = ThemeData(
    textTheme: TextTheme(
      titleLarge: TextStyle(
          color: dark.text,
          fontSize: 24,
          fontFamily: "Roboto",
          fontWeight: FontWeight.w500
      ),
      titleMedium: TextStyle(
          color: dark.subText,
          fontSize: 14,
          fontFamily: "Roboto",
          fontWeight: FontWeight.w400
      ),
      titleSmall: TextStyle(
          color: dark.disableTextAccent,
          fontSize: 16,
          fontFamily: "Roboto",
          fontWeight: FontWeight.w400
      ),
    ),
    filledButtonTheme: FilledButtonThemeData(
        style: FilledButton.styleFrom(
            textStyle: TextStyle(
                color: dark.disableTextAccent, fontWeight: FontWeight.w700,
                fontSize: 16
            ),
            backgroundColor: dark.accent,
            disabledBackgroundColor: dark.disableAccent,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4)
            ),
            padding: EdgeInsets.symmetric(horizontal: 36, vertical: 15)
        )
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
        style: OutlinedButton.styleFrom(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4)
            ),
            padding: EdgeInsets.symmetric(horizontal: 36, vertical: 15)
        )
    )
);