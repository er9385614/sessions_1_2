import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sessions_1_2/common/colors.dart';

class CustomTextField extends StatefulWidget{

  final String label;
  final String hint;
  final bool enableObscure;
  final TextEditingController controller;
  final bool isValid;

  const CustomTextField(
      {
        super.key,
        required this.label,
        required this.hint,
        this.enableObscure = false,
        required this.controller,
        this.isValid = true});
  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {

  bool isObscure = true;
  var colors = LightColors();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 24,),
        Text(widget.label,
        style: Theme.of(context).textTheme.titleMedium,),
        SizedBox(height: 8,),
        SizedBox(
          width: double.infinity,
          height: 44,
          child: TextField(
            controller: widget.controller,
            obscureText: (widget.enableObscure) ? isObscure : false,
            obscuringCharacter: "*",
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 14),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: colors.subText,
                  ),
                  borderRadius: BorderRadius.circular(4)
              ),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: colors.subText,
                  ),
                  borderRadius: BorderRadius.circular(4)
              ),
              border: OutlineInputBorder(
                borderSide: BorderSide(
                  color: colors.subText,
                ),
                borderRadius: BorderRadius.circular(4)
              ),
              hintStyle: Theme.of(context).textTheme.titleMedium?.copyWith(
                color: colors.hint
              ),
              hintText: widget.hint,
              suffixIcon: (widget.enableObscure) ?
                  GestureDetector(
                    onTap: (){
                      setState(() {
                        isObscure = !isObscure;
                      });
                    },
                    child: Image.asset("assets/eye-slash.png"),
                  ) : null
            ),
          ),
        )
      ],
    );
  }
}