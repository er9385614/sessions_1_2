import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sessions_1_2/common/colors.dart';
import 'package:sessions_1_2/common/theme.dart';

import '../auth/presentation/pages/sign_up_page.dart';

class MyApp extends StatefulWidget {

  var isLightTheme = true;

  void changeDifferentTheme(BuildContext context){
    isLightTheme = !isLightTheme;
    context.findAncestorStateOfType<_MyAppState>()!.onChangeTheme();
  }

  static MyApp of(BuildContext context){
    return context.findAncestorWidgetOfExactType<MyApp>()!;
  }

  ColorsApp getColorsApp(BuildContext context){
    return (isLightTheme) ? light : dark;
  }

  ThemeData getCurrentTheme(){
    return (isLightTheme) ? lightTheme : darkTheme;
  }

  MyApp({super.key});@override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  void onChangeTheme(){
    setState(() {

    });
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: widget.getCurrentTheme(),
      home: SignUpPage(),
    );
  }
}
