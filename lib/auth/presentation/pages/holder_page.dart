import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sessions_1_2/auth/presentation/pages/sign_up_page.dart';
import 'package:sessions_1_2/common/colors.dart';

class HolderPage extends StatefulWidget{
  @override
  State<HolderPage> createState() => _HolderPageState();
}

class _HolderPageState extends State<HolderPage> {
  @override
  Widget build(BuildContext context) {
    var colors = LightColors();
    return Scaffold(
      backgroundColor: colors.background,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Center(
          child: SizedBox(
            width: double.infinity,
            height: 46,
            child: FilledButton(
                onPressed: (){
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(
                          builder: (_) => SignUpPage()
                  ),
                          (route) => false);
                },
                child: Text("ВЫХОД")),
          ),
        ),
      ),
    );
  }
}