import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sessions_1_2/auth/domain/log_in_presenter.dart';
import 'package:sessions_1_2/auth/domain/new_password_presenter.dart';
import 'package:sessions_1_2/auth/presentation/pages/forgot_password_page.dart';
import 'package:sessions_1_2/auth/presentation/pages/holder_page.dart';
import 'package:sessions_1_2/auth/presentation/pages/log_in_page.dart';
import 'package:sessions_1_2/auth/presentation/pages/sign_up_page.dart';
import 'package:sessions_1_2/common/colors.dart';
import 'package:sessions_1_2/common/widgets/CustomTextField.dart';

class NewPasswordPage extends StatefulWidget{

  @override
  State<NewPasswordPage> createState() => _NewPasswordPageState();
}

class _NewPasswordPageState extends State<NewPasswordPage> {

  var confirmPassword = TextEditingController();
  var password = TextEditingController();

  var presenter = NewPasswordPresenter();
  bool obscurePassword = true;
  bool obscureConfirmPassword = true;

  @override
  Widget build(BuildContext context) {
    var colors = LightColors();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 83),
            Text("Новый пароль",
              style: Theme.of(context).textTheme.titleLarge,),
            SizedBox(height: 8,),
            Text("Введите новый пароль",
              style: Theme.of(context).textTheme.titleMedium,),
            CustomTextField(
                label: "Пароль",
                hint: "**********",
                controller: password,
                enableObscure: obscurePassword,),
            CustomTextField(
              label: "Подтвердить пароль",
              hint: "**********",
              controller: confirmPassword,
              enableObscure: obscureConfirmPassword,),
            SizedBox(height: 18,),
            Expanded(child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SizedBox(
                    width: double.infinity,
                    height: 46,
                    child: FilledButton(
                        onPressed: (){
                          presenter.pressUpdatePassword(
                              password.text,
                                  (_) => Navigator.of(context).pushAndRemoveUntil(
                                      MaterialPageRoute(builder: (_) => LogInPage()), (route) => false), (p0) => null);
                        },
                        child: Text("Подтвердить",))
                ),

                SizedBox(height: 14,),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).pushReplacement(
                        MaterialPageRoute(
                            builder: (_) => SignUpPage()
                        )
                    );
                  },
                  child: RichText(text: TextSpan(
                      children: [
                        TextSpan(text: "Я вспомнил свой пароль! ",
                            style: Theme.of(context).textTheme.titleMedium),
                        TextSpan(text: "Вернуться",
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                color: colors.accent
                            )),
                      ]
                  )),
                ),
                SizedBox(height: 32,)
              ],
            ))
          ],
        ),
      ),
    );
  }
}