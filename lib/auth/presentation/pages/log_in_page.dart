import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sessions_1_2/auth/domain/log_in_presenter.dart';
import 'package:sessions_1_2/auth/presentation/pages/forgot_password_page.dart';
import 'package:sessions_1_2/auth/presentation/pages/holder_page.dart';
import 'package:sessions_1_2/auth/presentation/pages/sign_up_page.dart';
import 'package:sessions_1_2/common/colors.dart';
import 'package:sessions_1_2/common/widgets/CustomTextField.dart';
import 'package:sessions_1_2/home/presentation/pages/home_page.dart';

class LogInPage extends StatefulWidget{

  @override
  State<LogInPage> createState() => _LogInPageState();
}

class _LogInPageState extends State<LogInPage> {

  var email = TextEditingController();
  var password = TextEditingController();

  var presenter = LogInPresenter();
  var isRememberMe = false;
  bool obscurePassword = true;

  @override
  Widget build(BuildContext context) {
    var colors = LightColors();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 83),
            Text("Добро пожаловать",
              style: Theme.of(context).textTheme.titleLarge,),
            SizedBox(height: 8,),
            Text("Заполните почту и пароль чтобы продолжить",
              style: Theme.of(context).textTheme.titleMedium,),
            CustomTextField(
                label: "Почта",
                hint: "***********@mail.com",
                controller: email),
            CustomTextField(
              label: "Пароль",
              hint: "**********",
              controller: password,
              enableObscure: obscurePassword,),
            SizedBox(height: 18,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    SizedBox.square(
                      dimension: 22,
                      child: Transform.scale(
                        scale: 1.2,
                        child: Checkbox(
                          activeColor: colors.accent,
                            side: BorderSide(
                                color: colors.subText
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)
                            ),
                            value: isRememberMe,
                            onChanged: (value){
                              setState(() {
                                isRememberMe = !isRememberMe;
                              });
                            }),
                      ),
                    ),
                    SizedBox(width: 8,),
                    Text("Запомнить меня",
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(
                      fontWeight: FontWeight.w500
                    ),)
                  ],
                ),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(
                            builder: (_) => ForgotPasswordPage()
                    ),
                            (route) => false);
                  },
                  child: Text("Забыли пароль?",
                  style: Theme.of(context).textTheme.titleMedium?.copyWith(
                    color: colors.accent, fontWeight: FontWeight.w500
                  ),),
                )
              ],
            ),
            Expanded(child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SizedBox(
                    width: double.infinity,
                    height: 46,
                    child: FilledButton(
                        onPressed: (){
                          presenter.pressLogIn(
                              email.text,
                              password.text,
                                  (_){Navigator.of(context).pushAndRemoveUntil(
                                  MaterialPageRoute(
                                      builder: (_) => const HomePage()
                                  ),
                                      (route) => false);}, (p0) => null);
                        },
                        child: Text("Войти",))
                ),

                SizedBox(height: 14,),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).pushReplacement(
                        MaterialPageRoute(
                            builder: (_) => SignUpPage()
                        )
                    );
                  },
                  child: RichText(text: TextSpan(
                      children: [
                        TextSpan(text: "У меня нет аккаунта! ",
                            style: Theme.of(context).textTheme.titleMedium),
                        TextSpan(text: "Создать",
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                color: colors.accent
                            )),
                      ]
                  )),
                ),
                SizedBox(height: 32,)
              ],
            ))
          ],
        ),
      ),
    );
  }
}