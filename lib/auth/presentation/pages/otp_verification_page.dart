import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pinput/pinput.dart';
import 'package:sessions_1_2/auth/domain/forgot_password_presenter.dart';
import 'package:sessions_1_2/auth/domain/log_in_presenter.dart';
import 'package:sessions_1_2/auth/domain/new_password_presenter.dart';
import 'package:sessions_1_2/auth/domain/otp_verify_presenter.dart';
import 'package:sessions_1_2/auth/presentation/pages/holder_page.dart';
import 'package:sessions_1_2/auth/presentation/pages/sign_up_page.dart';
import 'package:sessions_1_2/common/colors.dart';
import 'package:sessions_1_2/common/widgets/CustomTextField.dart';

import 'new_password_page.dart';

class OTPVerificationPage extends StatefulWidget{

  final String email;

  const OTPVerificationPage({super.key, required this.email});
  @override
  State<OTPVerificationPage> createState() => _OTPVerificationPageState();
}

class _OTPVerificationPageState extends State<OTPVerificationPage> {

  var code = TextEditingController();
  var presenter = OTPVerificationPresenter();

  @override
  Widget build(BuildContext context) {
    var colors = LightColors();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 83),
            Text("Верификация",
              style: Theme.of(context).textTheme.titleLarge,),
            SizedBox(height: 8,),
            Text("Введите 6-ти значный код из письма",
              style: Theme.of(context).textTheme.titleMedium,),
            SizedBox(height: 58,),
            Pinput(
              length: 6,
              controller: code,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              defaultPinTheme: PinTheme(
                width: 32,
                height: 32,
                decoration: BoxDecoration(
                  border: Border.all(color: colors.subText),
                  borderRadius: BorderRadius.circular(0),
                ),
              ),
              focusedPinTheme: PinTheme(
                width: 32,
                height: 32,
                decoration: BoxDecoration(
                  border: Border.all(color: colors.accent),
                  borderRadius: BorderRadius.circular(0),
                ),
              ),
            ),
            SizedBox(height: 18,),

            Expanded(child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SizedBox(
                    width: double.infinity,
                    height: 46,
                    child: FilledButton(
                        onPressed: (){
                          presenter.pressVerify(
                              widget.email,
                              code.text,
                                  (_) => Navigator.of(context).pushAndRemoveUntil(
                                      MaterialPageRoute(
                                          builder: (_) => NewPasswordPage()
                                      ),
                                          (route) => false), (p0) => null);
                        },
                        child: Text("Сбросить пароль",))
                ),

                SizedBox(height: 14,),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).pushReplacement(
                        MaterialPageRoute(
                            builder: (_) => SignUpPage()
                        )
                    );
                  },
                  child: RichText(text: TextSpan(
                      children: [
                        TextSpan(text: "Я вспомнил свой пароль! ",
                            style: Theme.of(context).textTheme.titleMedium),
                        TextSpan(text: "Вернуться",
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                color: colors.accent
                            )),
                      ]
                  )),
                ),
                SizedBox(height: 32,)
              ],
            ))
          ],
        ),
      ),
    );
  }
}