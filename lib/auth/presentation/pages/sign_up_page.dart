import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sessions_1_2/auth/domain/sign_up_presenter.dart';
import 'package:sessions_1_2/auth/presentation/pages/holder_page.dart';
import 'package:sessions_1_2/auth/presentation/pages/log_in_page.dart';
import 'package:sessions_1_2/common/colors.dart';
import 'package:sessions_1_2/common/widgets/CustomTextField.dart';
import 'package:sessions_1_2/home/presentation/pages/home_page.dart';

class SignUpPage extends StatefulWidget{

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {

  var presenter = SignUpPresenter();

  var email = TextEditingController();
  var password = TextEditingController();
  var confirmPassword = TextEditingController();

  bool obscurePassword = true;
  bool obscureConfirmPassword = true;

  @override
  Widget build(BuildContext context) {
    var colors = LightColors();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 83),
            Text("Создать аккаунт",
              style: Theme.of(context).textTheme.titleLarge,),
            SizedBox(height: 8,),
            Text("Завершите регистрацию чтобы начать",
              style: Theme.of(context).textTheme.titleMedium,),
            CustomTextField(
                label: "Почта",
                hint: "***********@mail.com",
                controller: email),
            CustomTextField(
                label: "Пароль",
                hint: "**********",
                controller: password,
                enableObscure: obscurePassword,),
            CustomTextField(
                label: "Повторите пароль",
                hint: "**********",
                controller: confirmPassword,
                enableObscure: obscureConfirmPassword,),
            Expanded(child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    width: double.infinity,
                    height: 46,
                    child: FilledButton(
                        onPressed: (){
                          presenter.pressSignUp(
                              email.text,
                              password.text,
                                  (_) => Navigator.of(context).pushAndRemoveUntil(
                                      MaterialPageRoute(
                                          builder: (_) => const HomePage()
                                  ),
                                          (route) => false), (p0) => null);
                        },
                        child: Text("Зарегистрироваться"))
                  ),

                  SizedBox(height: 14,),
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                              builder: (_) => LogInPage()
                          ),
                              (route) => false);
                    },
                    child: RichText(text: TextSpan(
                      children: [
                        TextSpan(text: "У меня уже есть аккаунт! ",
                        style: Theme.of(context).textTheme.titleMedium),
                        TextSpan(text: "Войти",
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(
                              color: colors.accent
                            )),
                      ]
                    )),
                  ),
                  SizedBox(height: 32,)
                ],
            ))
          ],
        ),
      ),
    );
  }
}