import 'package:gotrue/src/types/auth_response.dart';
import 'package:sessions_1_2/auth/data/repository/supabase.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../data/models/model_auth.dart';

class LogInPresenter{
  Future<void> pressLogIn(
      String email,
      String password,
      Function(AuthResponse) onResponse,
      Function(String) onError
      ) async {
    try{
      ModelAuth modelAuth = ModelAuth(email: email, password: password);
      dynamic result = await(logIn(modelAuth));
      onResponse(result);
    } on AuthException catch (e){
      onError(e.message);
    } on PostgrestException catch (e){
      onError(e.message);
    } on Exception catch (e){
      onError(e.toString());
    }
  }
}