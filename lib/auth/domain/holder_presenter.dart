import 'package:sessions_1_2/auth/data/repository/supabase.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class HolderPresenter{
  Future<void> pressLogOut(
      Function() onResponse,
      Function(String) onError
      ) async {
    try{
      logOut();
      onResponse();
    } on AuthException catch (e){
      onError(e.message);
    } on PostgrestException catch (e){
      onError(e.message);
    } on Exception catch (e){
      onError(e.toString());
    }
  }

}