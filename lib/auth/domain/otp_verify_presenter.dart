import 'package:sessions_1_2/auth/data/repository/supabase.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class OTPVerificationPresenter{

  Future<void> pressVerify(
      String email,
      String code,
      Function(AuthResponse) onResponse,
      Function(String) onError
      ) async {
    try{
      var result = await verifyOTP(email, code);
      onResponse(result);
    } on AuthException catch (e){
      onError(e.message);
    } on PostgrestException catch (e){
      onError(e.message);
    } on Exception catch (e){
      onError(e.toString());
    }
  }
}