
import 'package:sessions_1_2/auth/data/repository/supabase.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class NewPasswordPresenter{
  Future<void> pressUpdatePassword(
      String password,
      Function(UserResponse) onResponse,
      Function(String) onError,
      ) async {
    try{
      var result = await updatePassword(password);
      onResponse(result);
    } on AuthException catch (e){
      onError(e.message);
    } on PostgrestException catch (e){
      onError(e.message);
    } on Exception catch (e){
      onError(e.toString());
    }
  }
}